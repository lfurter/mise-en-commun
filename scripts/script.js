
/*$('a').smoothScroll();*/

$('html').click(function() {
    $(".news-box").hide()
});

$('.news-box').click(function(event){
   event.stopPropagation();
});

$(".changeimage").click(function () {
    $(this).find(".toggle1").toggle();
    $(this).find(".toggle2").toggle();
});

$( ".draggable" ).draggable();
$( "#ajouter div" ).draggable();

$(".click-blue").click(function () {
    $(".change").css("color", "blue");
});
$(".click-yellow").click(function () {
    $(".change").css("color", "yellow");
});
$(".click-black").click(function () {
    $(".change").css("color", "black");
});

/* DIAPO */

$(".diapo > img:gt(0)").hide();

setInterval(function() {
  $('.diapo > img:first')
    .fadeOut(0)
    .next()
    .fadeIn(0)
    .end()
    .appendTo('.diapo');
},  3000);

/* ADD NEW ITEM */

var btnAdd = document.getElementById('add');
btnAdd.addEventListener("click", addElement, false);

function addElement() {
    var olList = document.getElementById('ajouter');
    var newListItem = document.createElement('div');
    newListItem.innerText = 'plus';
    olList.appendChild(newListItem);
}

/* DROP IMAGES */

var holder = document.getElementById('holder'),
    tests = {
      filereader: typeof FileReader != 'undefined',
      dnd: 'draggable' in document.createElement('span'),
      formdata: !!window.FormData,
      progress: "upload" in new XMLHttpRequest
    },
    support = {
      filereader: document.getElementById('filereader'),
      formdata: document.getElementById('formdata'),
      progress: document.getElementById('progress')
    },
    acceptedTypes = {
      'image/png': true,
      'image/jpeg': true,
      'image/gif': true
    },
    progress = document.getElementById('uploadprogress'),
    fileupload = document.getElementById('upload');

"filereader formdata progress".split(' ').forEach(function (api) {
  if (tests[api] === false) {
    support[api].className = 'fail';
  } else {
    // FFS. I could have done el.hidden = true, but IE doesn't support
    // hidden, so I tried to create a polyfill that would extend the
    // Element.prototype, but then IE10 doesn't even give me access
    // to the Element object. Brilliant.
    support[api].className = 'hidden';
  }
});

function previewfile(file) {
  if (tests.filereader === true && acceptedTypes[file.type] === true) {
    var reader = new FileReader();
    reader.onload = function (event) {
      var image = new Image();
      image.src = event.target.result;
      image.width = 250; // a fake resize
      holder.appendChild(image);
    };

    reader.readAsDataURL(file);
  }  else {
    holder.innerHTML += '<p>Uploaded ' + file.name + ' ' + (file.size ? (file.size/1024|0) + 'K' : '');
    console.log(file);
  }
}

function readfiles(files) {
    debugger;
    var formData = tests.formdata ? new FormData() : null;
    for (var i = 0; i < files.length; i++) {
      if (tests.formdata) formData.append('file', files[i]);
      previewfile(files[i]);
    }

    // now post a new XHR request
    if (tests.formdata) {
      var xhr = new XMLHttpRequest();
      xhr.open('POST', '/devnull.php');
      xhr.onload = function() {
        progress.value = progress.innerHTML = 100;
      };

      if (tests.progress) {
        xhr.upload.onprogress = function (event) {
          if (event.lengthComputable) {
            var complete = (event.loaded / event.total * 100 | 0);
            progress.value = progress.innerHTML = complete;
          }
        }
      }

      xhr.send(formData);
    }
}

if (tests.dnd) {
  holder.ondragover = function () { this.className = 'hover'; return false; };
  holder.ondragend = function () { this.className = ''; return false; };
  holder.ondrop = function (e) {
    this.className = '';
    e.preventDefault();
    readfiles(e.dataTransfer.files);
  }
} else {
  fileupload.className = 'hidden';
  fileupload.querySelector('input').onchange = function () {
    readfiles(this.files);
  };
}
